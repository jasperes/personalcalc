#!/usr/bin/env python3

from personalCalc.core.database.instance import DB
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Integer, String


class Comprador(DB.Base):
    __tablename__ = 'compradores'

    id_comprador = Column(Integer, primary_key=True)
    nome = Column(String(75), nullable=False)
