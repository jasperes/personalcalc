#!/usr/bin/env python3

from personalCalc.core.database.instance import DB
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import Column, ForeignKey
from sqlalchemy.sql.sqltypes import Integer, String


class ModoPagamento(DB.Base):
    __tablename__ = 'modos_pagamento'

    id_modo_pagamento = Column(Integer, primary_key=True)
    id_pessoa = Column(Integer, ForeignKey('pessoas.id_pessoa'), nullable=False)
    descricao = Column(String(30), nullable=False)

    pessoa = relationship('Pessoa')
