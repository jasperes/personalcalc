#!/usr/bin/env python3

from personalCalc.core.models.comprador import Comprador
from personalCalc.core.models.conta import Conta
from personalCalc.core.models.divida import Divida
from personalCalc.core.models.modo_pagamento import ModoPagamento
from personalCalc.core.models.parametro import Parametro
from personalCalc.core.models.pessoa import Pessoa
from personalCalc.core.models.tipo_vendedor import TipoVendedor
from personalCalc.core.models.vendedor import Vendedor
