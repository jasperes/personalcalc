#!/usr/bin/env python3

from personalCalc.core.database.instance import DB
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import String, Integer


class TipoVendedor(DB.Base):
    __tablename__ = 'tipos_vendedores'

    id_tipo_vendedor = Column(Integer, primary_key=True)
    descricao = Column(String(15), nullable=False)
