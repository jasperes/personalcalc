#!/usr/bin/env python3

from personalCalc.core.database.instance import DB
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import Column, ForeignKey
from sqlalchemy.sql.sqltypes import String, Integer


class Vendedor(DB.Base):
    __tablename__ = 'vendedores'

    id_vendedor = Column(Integer, primary_key=True)
    id_tipo_vendedor = Column(Integer, ForeignKey('tipos_vendedores.id_tipo_vendedor'),  nullable=False)
    descricao = Column(String(50), nullable=False)

    tipo_vendedor = relationship('TipoVendedor')
