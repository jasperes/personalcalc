#!/usr/bin/env python3

from personalCalc.core.database.instance import DB
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import String, Integer


class Pessoa(DB.Base):
    __tablename__ = 'pessoas'

    id_pessoa = Column(Integer, primary_key=True)
    nome = Column(String(75), nullable=False)
