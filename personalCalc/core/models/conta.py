#!/usr/bin/env python3

from personalCalc.core.database.instance import DB
from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Integer, String, Date, Boolean, Float


class Conta(DB.Base):
    __tablename__ = 'contas'

    id_conta = Column(Integer, primary_key=True)
    descricao = Column(String(25), nullable=False)
    valor_parcela = Column(Float, nullable=False)
    quantidade_parcelas = Column(Integer, nullable=False)
    dia_pagamento_parcela = Column(Integer, nullable=False)
    data_primeiro_pagamento = Column(Date, nullable=False)
    quitado = Column(Boolean, nullable=False)
