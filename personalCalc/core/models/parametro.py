#!/usr/bin/env python3

from personalCalc.core.database.instance import DB
from sqlalchemy import Column, Integer, String


class Parametro(DB.Base):
    __tablename__ = 'parametros'

    id_parametro = Column(Integer, primary_key=True)
    nome = Column(String(50), nullable=False)
    valor = Column(String(50))
