#!/usr/bin/env python3

from personalCalc.core.database.instance import DB
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import Column, ForeignKey
from sqlalchemy.sql.sqltypes import Integer


class Divida(DB.Base):
    __tablename__ = 'dividas'

    id_divida = Column(Integer, primary_key=True)
    id_modo_pagamento = Column(Integer, ForeignKey('modos_pagamento.id_modo_pagamento'), nullable=False)
    id_comprador = Column(Integer, ForeignKey('compradores.id_comprador'), nullable=False)
    id_vendedor = Column(Integer, ForeignKey('vendedores.id_vendedor'))
    id_conta = Column(Integer, ForeignKey('contas.id_conta'), nullable=False)

    modo_pagamento = relationship('ModoPagamento')
    comprador = relationship('Comprador')
    vendedor = relationship('Vendedor')
    conta = relationship('Conta')
