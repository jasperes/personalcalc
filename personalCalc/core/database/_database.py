#!/usr/bin/env python3

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


class DataBase:

    def __init__(self):
        self.Base = declarative_base()
        self.engine = None
        self.session = None

    def _set_connection(self, connection):
        self.engine = create_engine(connection)

    def _initialize_session(self):
        session = sessionmaker()
        session.configure(bind=self.engine)
        self.session = session()

    def _create_tables(self):
        self.Base.metadata.create_all(self.engine)

    def initialize(self, connection):
        self._set_connection(connection)
        self._create_tables()
        self._initialize_session()
