#!/usr/bin/env python3

from configparser import ConfigParser

config = ConfigParser()

database = config['DATABASE'] = {}
database['connection'] = ''
database['server'] = ''
database['user'] = ''
database['password'] = ''
database['database'] = ''

def create(file_name):
    with open(file_name, 'w') as file:
        config.write(file)
